CREATE VIEW didactical_load AS (
  SELECT p.name AS individual, c.name AS course
  FROM persons p, courses c, course2person cp
  WHERE p.id = cp.person_id
  AND cp.course_id = c.id
);
