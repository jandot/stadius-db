INSERT INTO academic_functions (name) VALUES ("professor");
INSERT INTO academic_functions (name) VALUES ("postdoc");
INSERT INTO academic_functions (name) VALUES ("senior_researcher");
INSERT INTO academic_functions (name) VALUES ("phd");
INSERT INTO academic_functions (name) VALUES ("research_manager");
INSERT INTO academic_functions (name) VALUES ("technology_developer");
INSERT INTO academic_functions (name) VALUES ("business_developer");
INSERT INTO academic_functions (name) VALUES ("research_expert");
