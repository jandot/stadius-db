INSERT INTO research_groups (acronym, name) VALUES ("BIOI", "Bioinformatics");
INSERT INTO research_groups (acronym, name) VALUES ("BIOMED", "Biomedical Data Processing");
INSERT INTO research_groups (acronym, name) VALUES ("SMC", "Systems, Models and Control");
INSERT INTO research_groups (acronym, name) VALUES ("DSP", "Digital Signal Processing for Audio and Telecommunications");