CREATE TABLE topics (
  id INTEGER PRIMARY KEY,
  name STRING
);