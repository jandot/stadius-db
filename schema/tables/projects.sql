CREATE TABLE projects (
  id INTEGER PRIMARY KEY,
  name STRING,
  funding_agency_id INTEGER,
  number STRING,
  status_id INTEGER
);