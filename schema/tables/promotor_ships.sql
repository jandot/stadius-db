CREATE TABLE promotor_ships (
  id INTEGER PRIMARY KEY,
  person_id INTEGER,
  project_id INTEGER,
  role STRING
);