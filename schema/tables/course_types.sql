CREATE TABLE course_types (
  id INTEGER PRIMARY KEY,
  name STRING
);