CREATE TABLE courses (
  id INTEGER PRIMARY KEY,
  name STRING,
  course_type_id INTEGER,
  coordinator person_id
);