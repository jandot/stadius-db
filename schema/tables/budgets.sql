CREATE TABLE budgets (
  id INTEGER PRIMARY KEY,
  project_id INTEGER,
  start_amount FLOAT,
  running_amount FLOAT,
  start_date DATE
);