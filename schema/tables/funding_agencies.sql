CREATE TABLE funding_agencies (
  id INTEGER PRIMARY KEY,
  name STRING
);