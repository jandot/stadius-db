CREATE TABLE research_groups (
  id INTEGER PRIMARY KEY,
  acronym STRING,
  name STRING
);