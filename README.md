# Stadius-DB

This repository hosts the code for the database and interface to Stadius-DB.

## Organization of the code

### Best practices

* All table names are in **plural**
* Each table has a unique primary key called **id**
* **Foreign key** names consist of the singular of the table name, followed by ``_id``. For example, a foreign key that links to a record in the ``persons`` table should be name ``person_id``.
* There is one exception to these rules, namely when there's a **many-to-many** relationship. An example is the link between individuals and courses. A single individual can teach multiple courses, and each course can have multiple people involved. In that case, we use separate tables linking these. The name of such table that links table_a and table_b is ``table_a2table_b``, where we order the tables in alphabetical order. In the individuals-to-courses example: ``courses2individuals`` (instead of ``individuals2courses``).

### Directory structure

Two top-directories hold the code for (a) the backend database, and (b) the PHP interface.

    stadius-db
      +- schema
      |    +- tables
      |    |    +- budgets.sql
      |    |    +- course2individual.sql
      |    |    +- ...
      |    +- views
      |         +- didactic_load.sql
      |         +- ...
      +- interface
           +- ...
           +- ...